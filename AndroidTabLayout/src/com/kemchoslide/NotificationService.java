package com.kemchoslide;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.IBinder;
import android.util.Log;

public class NotificationService extends Service {

	NotificationManager nm;
	static final int uniqueID = 11;

	private static String url_create_product = "";
	private static String url_userinfo = "";
	JSONParser jsonParser = new JSONParser();
	ArrayList<String> senderid = new ArrayList<String>();
	ArrayList<String> messagelist = new ArrayList<String>();
	ArrayList<String> firstname = new ArrayList<String>();

	String authkey;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		nm = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		nm.cancel(uniqueID);

		Log.i("Notification---> ", "notification Started");

		Thread timer = new Thread() {

			public void run() {

				while (true)
					try {
						sleep(10000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {

						String status = null;
						List<NameValuePair> params = new ArrayList<NameValuePair>();
						SharedPreferences setting = getSharedPreferences(
								"login", 0);
						authkey = setting.getString("authkey", "nothing");

						params.add(new BasicNameValuePair("_ses", authkey));

						JSONObject json = jsonParser.makeHttpRequest(
								"http://chat.scubeg.com/api/me/alerts", "GET",
								params);

						try {
							status = json.getString("status");
							if (status.equals("success")) {

								String array = json.getString("data");

								JSONArray responcearray = new JSONArray(array);
								for (int i = 0; i < responcearray.length(); i++) {
									JSONObject jObject = responcearray
											.getJSONObject(i);
									String message = jObject
											.getString("message");
									String sender_id = jObject
											.getString("sender_id");
									String sender_firstname = jObject
											.getString("sender_firstname");
									firstname.add(sender_firstname);

									senderid.add(sender_id);
									messagelist.add(message);

									Log.i("message", "" + message);

								}

							}

						} catch (JSONException e) {
							e.printStackTrace();
						}

						removeDuplicates(firstname);
						removeDuplicates(senderid);

						for (int kk = 0; kk < senderid.size(); kk++) {

							Intent intent = new Intent(
									NotificationService.this,
									ChatTabActivity.class);
							intent.putExtra("groupid", senderid.get(kk));
							PendingIntent pi = PendingIntent.getActivity(
									NotificationService.this, 0, intent, 0);
							String body = "You Got New Message";
							String title = firstname.get(kk);

							Notification n = new Notification(
									R.drawable.logo_title, body,
									System.currentTimeMillis());
							n.setLatestEventInfo(NotificationService.this,
									title, body, pi);
							n.defaults = Notification.DEFAULT_ALL;
							nm.notify(Integer.parseInt(senderid.get(kk)), n);
							n.flags |= Notification.FLAG_AUTO_CANCEL;

						}
						Delete_All_Alert();

					}

			}

			private void Delete_All_Alert() {

				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("_ses", authkey));
				params.add(new BasicNameValuePair("_action", "clear_all_alerts"));
				// {'_action':'clear_all_alerts'}
				JSONObject json = jsonParser.makeHttpRequest(
						"http://chat.scubeg.com", "POST", params);
				senderid.removeAll(senderid);
			}

		};
		timer.start();

		return super.onStartCommand(intent, flags, startId);
	}

	private static void generateNotification(Context context, String message) {
		int icon = R.drawable.ic_launcher;
		long when = System.currentTimeMillis();
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Notification notification = new Notification(icon, message, when);

		String title = context.getString(R.string.app_name);

		Intent notificationIntent = new Intent(context, Login.class);
		// set intent so it does not start a new activity
		notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP
				| Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent intent = PendingIntent.getActivity(context, 0,
				notificationIntent, 0);
		notification.setLatestEventInfo(context, title, message, intent);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;

		// Play default notification sound
		// notification.defaults |= Notification.DEFAULT_SOUND;

		// notification.sound = Uri.parse("android.resource://" +
		// context.getPackageName() + "your_sound_file_name.mp3");

		// Vibrate if vibrate is enabled
		notification.defaults |= Notification.DEFAULT_VIBRATE;
		notificationManager.notify(0, notification);
	}

	public static <T> void removeDuplicates(ArrayList<T> list) {
		int size = list.size();
		int out = 0;
		{
			final Set<T> encountered = new HashSet<T>();
			for (int in = 0; in < size; in++) {
				final T t = list.get(in);
				final boolean first = encountered.add(t);
				if (first) {
					list.set(out++, t);
				}
			}
		}
		while (out < size) {
			list.remove(--size);
		}
	}
}
