package com.kemchoslide;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

public class Splash extends Activity implements AnimationListener {

	private static String url_create_product = "";
	JSONParser jsonParser = new JSONParser();
	String username;
	String password;
	String authkey;
	String edituser, editpass;
	// UpdateContacts updatecontact;

	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	Animation animBounce;
	Domain domain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.splash);

		domain = new Domain();
		url_create_product = domain.getMainDomain(0);
		// updatecontact = new UpdateContacts(this);
		//
		// updatecontact.runcontacts();

		// nitification service
		Intent notificationserice = new Intent(Splash.this,
				NotificationService.class);
		startService(notificationserice);

		animBounce = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.rotate);
		animBounce.setAnimationListener(Splash.this);
		ImageView Image = (ImageView) findViewById(R.id.imageView2);

		Image.startAnimation(animBounce);

		SharedPreferences setting = getSharedPreferences("automaticlogin", 0);
		edituser = setting.getString("phone", "n");
		editpass = setting.getString("pass", "n");
		cd = new ConnectionDetector(getApplicationContext());

		isInternetPresent = cd.isConnectingToInternet();

		if (!isInternetPresent) {
			showAlertDialog(Splash.this, "No Internet Connection",
					"You don't have internet connection.", false);

		} else {
			if (edituser.length() < 2) {
				Intent Login = new Intent(Splash.this, Login.class);
				startActivity(Login);
			} else {
				new CreateNewProduct().execute();
			}
		}
	}

	class CreateNewProduct extends AsyncTask<String, String, String> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected String doInBackground(String... args) {
			String status = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("_action", "login"));
			params.add(new BasicNameValuePair("phone", edituser));
			params.add(new BasicNameValuePair("password", editpass));
			JSONObject json = jsonParser.makeHttpRequest(url_create_product,
					"POST", params);
			try {
				status = json.getString("status");
				authkey = json.getString("authkey");

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (result.equals("success")) {
				Intent intent = new Intent(Splash.this, ChatTabActivity.class);
				startActivity(intent);

				SharedPreferences Automaticlogin = getSharedPreferences(
						"automaticlogin", 0);
				SharedPreferences.Editor Automaticloginedit = Automaticlogin
						.edit();
				Automaticloginedit.putString("phone", edituser);
				Automaticloginedit.putString("pass", editpass);

				Automaticloginedit.commit();

			} else {
				Toast.makeText(getBaseContext(), "UserName/Password is Wrong",
						Toast.LENGTH_SHORT).show();
			}

			SharedPreferences setting = getSharedPreferences("login", 0);
			SharedPreferences.Editor editor = setting.edit();
			editor.putString("name", username);
			editor.putString("pass", password);
			editor.putString("authkey", authkey);
			editor.commit();

			new userdata().execute();
		}
	}

	class userdata extends AsyncTask<String, String, String> {
		String user_phoneno;
		String User_photoLink = null;

		protected String doInBackground(String... args) {
			String status = null;
			List<NameValuePair> userpramas = new ArrayList<NameValuePair>();
			userpramas.add(new BasicNameValuePair("_ses", "" + authkey));

			JSONObject json = jsonParser.makeHttpRequest(url_create_product
					+ "/api/user/me", "GET", userpramas);

			try {
				status = json.getString("status");
				String userdata = json.getString("user_info");
				JSONObject userdatajson = new JSONObject(userdata);
				user_phoneno = userdatajson.getString("primary_phone");
				User_photoLink = userdatajson.getString("profile_pic");
				Log.i("User phone no -->", "" + user_phoneno);

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return user_phoneno;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			SharedPreferences setting = getSharedPreferences("userinfo", 0);
			SharedPreferences.Editor editor = setting.edit();
			editor.putString("phone", result);
			editor.commit();

		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.ic_launcher
				: R.drawable.ic_launcher);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		alertDialog.show();
	}

	public void onAnimationEnd(Animation animation) {

	}

	public void onAnimationRepeat(Animation animation) {
	}

	public void onAnimationStart(Animation animation) {
	}

}
