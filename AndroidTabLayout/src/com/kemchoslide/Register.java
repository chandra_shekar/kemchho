package com.kemchoslide;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

@SuppressLint("NewApi")
public class Register extends Activity implements OnClickListener {

	private Uri mImageCaptureUri;
	private ImageView mImageView;
	private AlertDialog dialog;

	Bitmap bitmapOrg;

	InputStream is;
	private static final int PICK_FROM_CAMERA = 1;
	private static final int CROP_FROM_CAMERA = 2;
	private static final int PICK_FROM_FILE = 3;

	private static String url_create_product = "http://chat.scubeg.com/";
	JSONParser jsonParser = new JSONParser();

	String errormessage, regID;
	EditText firstname, lastname, email, phone, password, dob;
	ToggleButton tb;

	static int count = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.regestration);

		initialize();
		captureImageInitialization();

		TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String getSimNumber = telemamanger.getLine1Number();
		phone.setText(getSimNumber);

	}

	private void initialize() {
		firstname = (EditText) findViewById(R.id.regetfirstname);
		lastname = (EditText) findViewById(R.id.regetsecondname);
		email = (EditText) findViewById(R.id.regetemailid);
		phone = (EditText) findViewById(R.id.regetphoneno);
		password = (EditText) findViewById(R.id.regetPassword);
		dob = (EditText) findViewById(R.id.regetdob);
		 dob.setOnClickListener(this);
		tb = (ToggleButton) findViewById(R.id.toggleButton1);
		tb.setOnClickListener(this);

		mImageView = (ImageView) findViewById(R.id.ivprofile);
		mImageView.setOnClickListener(this);
		Button register = (Button) findViewById(R.id.btregsignup);
		register.setOnClickListener(this);

	}

	@SuppressLint("NewApi")
	public void onClick(View v) {

		switch (v.getId()) {
		case R.id.btregsignup:

			if (firstname.getText().toString().length() == 0) {
				firstname.setError("Enter First Name");

			} else if (lastname.getText().toString().length() == 0) {
				lastname.setError("Enter First Name");

			} else if (email.getText().toString().length() == 0) {
				email.setError("Enter First Name");

			} else if (phone.getText().toString().length() == 0) {
				phone.setError("Enter First Name");

			} else if (password.getText().toString().length() == 0) {
				password.setError("Enter First Name");

			} else {
				new CreateNewProduct().execute(firstname.getText().toString(),
						lastname.getText().toString(), email.getText()
								.toString(), phone.getText().toString(),
						password.getText().toString());
			}

			break;
		case R.id.ivprofile:
			dialog.show();

			break;

		case R.id.regetdob:

			 DialogFragment newFragment = new SelectDateFragment();
			 newFragment.show(getFragmentManager(), "DatePicker");
			break;

		case R.id.toggleButton1:

			if (tb.isChecked()) {

				password.setTransformationMethod(null);
			} else {
				password.setTransformationMethod(new PasswordTransformationMethod());
			}

			break;

		}
	}

	class CreateNewProduct extends AsyncTask<String, String, String> {

		JSONArray contacts = null;

		protected String doInBackground(String... args) {
			String status = null;
			// Building Parameters
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			status = "fail";
			params.add(new BasicNameValuePair("_action", "signup"));
			params.add(new BasicNameValuePair("first_name", firstname.getText()
					.toString()));
			params.add(new BasicNameValuePair("last_name", lastname.getText()
					.toString()));
			params.add(new BasicNameValuePair("email", email.getText()
					.toString()));
			params.add(new BasicNameValuePair("phone", phone.getText()
					.toString()));
			params.add(new BasicNameValuePair("password", password.getText()
					.toString()));
			params.add(new BasicNameValuePair("device_id", phone.getText()
					.toString()));
			JSONObject json = jsonParser.makeHttpRequest(url_create_product,
					"POST", params);

			try {
				status = json.getString("status");
				if (status.equals("failure")) {
					errormessage = json.getString("error");
				}

				Log.i("new status", "" + status + " This is Error Message:"
						+ " ");

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			Toast.makeText(getBaseContext(),
					"register  " + result + errormessage, Toast.LENGTH_SHORT)
					.show();
			finish();
		}
	}

	private void captureImageInitialization() {
		/**
		 * a selector dialog to display two image source options, from camera
		 * �Take from camera� and from existing files �Select from gallery�
		 */
		final String[] items = new String[] { "Take from camera",
				"Select from gallery" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
				android.R.layout.select_dialog_item, items);
		AlertDialog.Builder builder = new AlertDialog.Builder(this);

		builder.setTitle("Select Image");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) { // pick from
																	// camera
				if (item == 0) {
					/**
					 * To take a photo from camera, pass intent action
					 * �MediaStore.ACTION_IMAGE_CAPTURE� to open the camera app.
					 */
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

					/**
					 * Also specify the Uri to save the image on specified path
					 * and file name. Note that this Uri variable also used by
					 * gallery app to hold the selected image path.
					 */
					mImageCaptureUri = Uri.fromFile(new File(Environment
							.getExternalStorageDirectory(), "tmp_avatar_"
							+ String.valueOf(System.currentTimeMillis())
							+ ".jpg"));

					intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
							mImageCaptureUri);

					try {
						intent.putExtra("return-data", true);

						startActivityForResult(intent, PICK_FROM_CAMERA);
					} catch (ActivityNotFoundException e) {
						e.printStackTrace();
					}
				} else {
					// pick from file
					/**
					 * To select an image from existing files, use
					 * Intent.createChooser to open image chooser. Android will
					 * automatically display a list of supported applications,
					 * such as image gallery or file manager.
					 */
					Intent intent = new Intent();

					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);

					startActivityForResult(Intent.createChooser(intent,
							"Complete action using"), PICK_FROM_FILE);
				}
			}
		});

		dialog = builder.create();
	}

	public class CropOptionAdapter extends ArrayAdapter<CropOption> {
		private ArrayList<CropOption> mOptions;
		private LayoutInflater mInflater;

		public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
			super(context, R.layout.crop_selector, options);

			mOptions = options;

			mInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup group) {
			if (convertView == null)
				convertView = mInflater.inflate(R.layout.crop_selector, null);

			CropOption item = mOptions.get(position);

			if (item != null) {
				((ImageView) convertView.findViewById(R.id.iv_icon))
						.setImageDrawable(item.icon);
				((TextView) convertView.findViewById(R.id.tv_name))
						.setText(item.title);

				return convertView;
			}

			return null;
		}
	}

	public class CropOption {
		public CharSequence title;
		public Drawable icon;
		public Intent appIntent;
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode != RESULT_OK)
			return;

		switch (requestCode) {
		case PICK_FROM_CAMERA:
			/**
			 * After taking a picture, do the crop
			 */
			doCrop();

			break;

		case PICK_FROM_FILE:
			/**
			 * After selecting image from files, save the selected path
			 */
			mImageCaptureUri = data.getData();

			doCrop();

			break;

		case CROP_FROM_CAMERA:
			Bundle extras = data.getExtras();
			/**
			 * After cropping the image, get the bitmap of the cropped image and
			 * display it on imageview.
			 */
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");

				mImageView.setImageBitmap(photo);
				bitmapOrg = photo;
				UploadPhoto();
				save("me");

				Toast.makeText(getBaseContext(), "Thanks For Uploading Photo!",
						Toast.LENGTH_SHORT).show();

			}

			File f = new File(mImageCaptureUri.getPath());
			/**
			 * Delete the temporary image
			 */
			if (f.exists())
				f.delete();

			break;

		}
	}

	private void doCrop() {
		final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
		/**
		 * Open image crop app by starting an intent
		 * �com.android.camera.action.CROP�.
		 */
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		/**
		 * Check if there is image cropper app installed.
		 */
		List<ResolveInfo> list = getPackageManager().queryIntentActivities(
				intent, 0);

		int size = list.size();

		/**
		 * If there is no image cropper app, display warning message
		 */
		if (size == 0) {

			Toast.makeText(this, "Can not find image crop app",
					Toast.LENGTH_SHORT).show();

			return;
		} else {
			/**
			 * Specify the image path, crop dimension and scale
			 */
			intent.setData(mImageCaptureUri);

			intent.putExtra("outputX", 200);
			intent.putExtra("outputY", 200);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);
			/**
			 * There is posibility when more than one image cropper app exist,
			 * so we have to check for it first. If there is only one app, open
			 * then app.
			 */

			if (size == 1) {
				Intent i = new Intent(intent);
				ResolveInfo res = list.get(0);

				i.setComponent(new ComponentName(res.activityInfo.packageName,
						res.activityInfo.name));

				startActivityForResult(i, CROP_FROM_CAMERA);
			} else {
				/**
				 * If there are several app exist, create a custom chooser to
				 * let user selects the app.
				 */
				for (ResolveInfo res : list) {
					final CropOption co = new CropOption();

					co.title = getPackageManager().getApplicationLabel(
							res.activityInfo.applicationInfo);
					co.icon = getPackageManager().getApplicationIcon(
							res.activityInfo.applicationInfo);
					co.appIntent = new Intent(intent);

					co.appIntent
							.setComponent(new ComponentName(
									res.activityInfo.packageName,
									res.activityInfo.name));

					cropOptions.add(co);
				}

				CropOptionAdapter adapter = new CropOptionAdapter(
						getApplicationContext(), cropOptions);

				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setTitle("Choose Crop App");
				builder.setAdapter(adapter,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								startActivityForResult(
										cropOptions.get(item).appIntent,
										CROP_FROM_CAMERA);
							}
						});

				builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
					public void onCancel(DialogInterface dialog) {

						if (mImageCaptureUri != null) {
							getContentResolver().delete(mImageCaptureUri, null,
									null);
							mImageCaptureUri = null;
						}
					}
				});

				AlertDialog alert = builder.create();

				alert.show();
			}
		}
	}

	private void UploadPhoto() {
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 90, bao);
		byte[] ba = bao.toByteArray();
		String ba1 = Base64.encodeBytes(ba);
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		SharedPreferences setting = getSharedPreferences("login", 0);
		String authkey = setting.getString("authkey", "nothing");

		nameValuePairs.add(new BasicNameValuePair("_action",
				"upload_profile_image"));
		nameValuePairs.add(new BasicNameValuePair("image", ba1));
		nameValuePairs.add(new BasicNameValuePair("_ses", authkey));

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://chat.scubeg.com");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());

		}

	}

	public void save(String filename) {
		// String filename;
		// Date date = new Date(0);
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		// filename = sdf.format(date);

		try {

			String path = Environment.getExternalStorageDirectory().toString();
			FileOutputStream fOut = null;
			File file = new File(path, "/DCIM/" + filename + ".jpg");
			fOut = new FileOutputStream(file);

			bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
			fOut.flush();
			fOut.close();

			MediaStore.Images.Media.insertImage(getContentResolver(),
					file.getAbsolutePath(), file.getName(), file.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressLint("ValidFragment")
	public void populateSetDate(int year, int month, int day) {
		dob.setText(month + "/" + day + "/" + year);
	}

	@SuppressLint("ValidFragment")
	public class SelectDateFragment extends DialogFragment implements
			DatePickerDialog.OnDateSetListener {
		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {
			final Calendar calendar = Calendar.getInstance();
			int yy = calendar.get(Calendar.YEAR);
			int mm = calendar.get(Calendar.MONTH);
			int dd = calendar.get(Calendar.DAY_OF_MONTH);
			return new DatePickerDialog(getActivity(), this, yy, mm, dd);
		}

		public void onDateSet(DatePicker view, int yy, int mm, int dd) {
			populateSetDate(yy, mm + 1, dd);
		}
	}

}
