package com.kemchoslide;

public class ProfileSlideData {

	public ProfileSlideData(String itemname) {
		super();
		this.itemname = itemname;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	String itemname;

}
