package com.kemchoslide;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class ForgotPassword extends Activity implements OnClickListener {

	private static String url_create_product = "";
	JSONParser jsonParser = new JSONParser();

	EditText email, phone;
	Domain domain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.forgotpass);
		
		domain = new Domain();
		url_create_product = domain.getMainDomain(0);

		email = (EditText) findViewById(R.id.forgotemail);
		phone = (EditText) findViewById(R.id.forgetphoneno);
		Button sendmail = (Button) findViewById(R.id.btSendMail);
		sendmail.setOnClickListener(this);

	}

	public void onClick(View arg0) {
		new forgotpassword().execute();

	}

	class forgotpassword extends AsyncTask<String, String, String> {
		String user_phoneno;

		protected String doInBackground(String... args) {
			String status = null;

			
//			{'_action':'forgot_password','mobile':'xxxxxxxxx','email':'xxx@yy.com'}
			
			List<NameValuePair> userpramas = new ArrayList<NameValuePair>();
			userpramas.add(new BasicNameValuePair("_action", "forgot_password"));
			userpramas.add(new BasicNameValuePair("mobile",""+phone.getText().toString()));
			userpramas.add(new BasicNameValuePair("email",""+email.getText().toString()));

			JSONObject json = jsonParser.makeHttpRequest(url_create_product,
					"POST", userpramas);

			return user_phoneno;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

		}
	}

}
