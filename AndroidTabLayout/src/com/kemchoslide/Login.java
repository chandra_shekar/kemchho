package com.kemchoslide;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity implements OnClickListener {

	private static String url_create_product = "";
	private static String url_userinfo = "";
	JSONParser jsonParser = new JSONParser();

	String username, password, authkey, edituser, editpass;
	EditText etuser, etpass;
	Boolean isInternetPresent = false;
	ConnectionDetector cd;
	Domain domain;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.login);

		domain = new Domain();
		url_create_product = domain.getMainDomain(0);
		url_userinfo = domain.getuserinfo(0);

		etuser = (EditText) findViewById(R.id.etphoneno);
		etpass = (EditText) findViewById(R.id.etPassword);
		Button login = (Button) findViewById(R.id.btnSignIn);
		login.setOnClickListener(this);
		Button register = (Button) findViewById(R.id.btnRegistration);
		register.setOnClickListener(this);
		Button forgotpass = (Button) findViewById(R.id.btnForgotPassword);
		forgotpass.setOnClickListener(this);

		TelephonyManager telemamanger = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		String getSimNumber = telemamanger.getLine1Number();
		etuser.setText("" + getSimNumber);

	}

	public void onClick(View v) {

		switch (v.getId()) {

		case R.id.btnForgotPassword:
			Intent forgot = new Intent(Login.this, ForgotPassword.class);
			startActivity(forgot);

			break;

		case R.id.btnRegistration:
			Intent register = new Intent(Login.this, Register.class);
			startActivity(register);
			break;
		case R.id.btnSignIn:
			cd = new ConnectionDetector(getApplicationContext());

			isInternetPresent = cd.isConnectingToInternet();

			if (!isInternetPresent) {
				showAlertDialog(Login.this, "No Internet Connection",
						"You don't have internet connection.", false);

			} else {
				{

					if (etuser.getText().toString().length() == 0) {
						etuser.setError("Put Name");
					} else if (etpass.getText().toString().length() == 0) {
						etpass.setError("put Password");
					} else {

						new CreateNewProduct().execute();
					}
				}
			}

			break;
		}
	}

	class CreateNewProduct extends AsyncTask<String, String, String> {

		JSONArray contacts = null;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
		}

		protected String doInBackground(String... args) {
			String status = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			edituser = etuser.getText().toString();
			editpass = etpass.getText().toString();

			params.add(new BasicNameValuePair("_action", "login"));
			params.add(new BasicNameValuePair("phone", edituser));
			params.add(new BasicNameValuePair("password", editpass));

			JSONObject json = jsonParser.makeHttpRequest(url_create_product,
					"POST", params);

			try {
				status = json.getString("status");
				authkey = json.getString("authkey");

			} catch (JSONException e) {
				e.printStackTrace();
			}

			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (result.equals("success")) {
				Toast.makeText(getBaseContext(), "Sucess..", Toast.LENGTH_SHORT)
						.show();
				Intent intent = new Intent(Login.this,
						ChatTabActivity.class);
				startActivity(intent);

				SharedPreferences Automaticlogin = getSharedPreferences(
						"automaticlogin", 0);
				SharedPreferences.Editor Automaticloginedit = Automaticlogin
						.edit();
				Automaticloginedit.putString("phone", edituser);
				Automaticloginedit.putString("pass", editpass);

				Automaticloginedit.commit();

			} else {
				Toast.makeText(getBaseContext(), "UserName/Password is Wrong",
						Toast.LENGTH_SHORT).show();
			}

			SharedPreferences setting = getSharedPreferences("login", 0);
			SharedPreferences.Editor editor = setting.edit();
			editor.putString("name", username);
			editor.putString("pass", password);
			editor.putString("authkey", authkey);
			editor.commit();

			new userdata().execute();
		}
	}

	class userdata extends AsyncTask<String, String, String> {
		String user_phoneno;

		protected String doInBackground(String... args) {
			String status = null;
			List<NameValuePair> userpramas = new ArrayList<NameValuePair>();
			userpramas.add(new BasicNameValuePair("_ses", "" + authkey));

			JSONObject json = jsonParser.makeHttpRequest(url_userinfo, "GET",
					userpramas);

			try {
				status = json.getString("status");
				String userdata = json.getString("user_info");
				JSONObject userdatajson = new JSONObject(userdata);
				user_phoneno = userdatajson.getString("primary_phone");

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return user_phoneno;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			SharedPreferences setting = getSharedPreferences("userinfo", 0);
			SharedPreferences.Editor editor = setting.edit();
			editor.putString("phone", result);
			editor.commit();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		finish();
	}

	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.ic_launcher
				: R.drawable.ic_launcher);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				finish();
			}
		});
		alertDialog.show();
	}

}
