package com.kemchoslide;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ActionBar.Tab;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.kemchoslide.ChatTabActivity.myListAdapter;
import com.kemchoslide.adapter.Me_TabsPagerAdapter;

public class MeTabActivity extends SlidingFragmentActivity implements
		ActionBar.TabListener {

	private ViewPager viewPager;
	private Me_TabsPagerAdapter mAdapter;
	private ActionBar actionBar;
	// Tab titles
	private String[] tabs = { "Profile", "Education", "Work", "Intrest" };

	// profile slide
	private List<ProfileSlideData> myCars = new ArrayList<ProfileSlideData>();
	ListView listback;
	ArrayAdapter<ProfileSlideData> adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// for slieding menu
		setBehindContentView(R.layout.profileslide);

		//
		listback = (ListView) findViewById(R.id.lvprofileslide);
		myCars.add(new ProfileSlideData("Profile"));
		myCars.add(new ProfileSlideData("Contacts"));
		myCars.add(new ProfileSlideData("Chats"));
		myCars.add(new ProfileSlideData("Logout"));

		adapter = new myListAdapter();
		listback.setAdapter(adapter);

		listback.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {

				Intent startaction;

				switch (pos) {
				case 0:
					toggle();

					break;

				case 1:
					startaction = new Intent(MeTabActivity.this,
							FriendsTabActivity.class);
					startActivity(startaction);

					finish();
					break;

				case 2:
					startaction = new Intent(MeTabActivity.this,
							ChatTabActivity.class);
					startActivity(startaction);

					finish();
					break;

				case 3:

					showAlertDialog(getBaseContext(), "Logout",
							"R u Sure Logout", false);

					break;

				}

			}
		});

		// start Notification service

		

		getActionBar().setDisplayHomeAsUpEnabled(true);

		SlidingMenu sm = getSlidingMenu();
		sm.setBehindOffset(100);
		sm.setShadowWidth(50);
		sm.setShadowDrawable(R.drawable.shadowgradient);
		sm.setSlidingEnabled(true);


		// Initilization
		viewPager = (ViewPager) findViewById(R.id.pager);
		actionBar = getActionBar();
		mAdapter = new Me_TabsPagerAdapter(getSupportFragmentManager());

		viewPager.setAdapter(mAdapter);
		actionBar.setHomeButtonEnabled(false);
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Adding Tabs
		for (String tab_name : tabs) {
			actionBar.addTab(actionBar.newTab().setText(tab_name)
					.setTabListener(this));
		}

		/**
		 * on swiping the viewpager make respective tab selected
		 * */
		viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

			@Override
			public void onPageSelected(int position) {
				// on changing the page
				// make respected tab selected
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}

			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});

		toggle();
	}

	@Override
	public void onTabReselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		// on tab selected
		// show respected fragment view
		viewPager.setCurrentItem(tab.getPosition());
	}

	@Override
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case android.R.id.home:
			Toast.makeText(getBaseContext(), "Home Clicked", Toast.LENGTH_SHORT)
					.show();

			toggle();
			break;
		case R.id.action_refresh:

			toggle();
			break;

		}
		return true;
	}

	@Override
	protected void onPause() {
		super.onPause();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// getMenuInflater().inflate(R.menu.main, menu);

		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.menucool, menu);
		return true;
	}

	public void showAlertDialog(Context context, String title, String message,
			Boolean status) {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(
				MeTabActivity.this);

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		// alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.logo_title);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("YES",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						SharedPreferences Automaticlogin = getSharedPreferences(
								"automaticlogin", 0);
						SharedPreferences.Editor Automaticloginedit = Automaticlogin
								.edit();
						Automaticloginedit.putString("phone", "");
						Automaticloginedit.putString("pass", "");
						Automaticloginedit.commit();
						Intent login = new Intent(MeTabActivity.this,
								Login.class);
						startActivity(login);
						finish();
					}
				});

		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("NO",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// Write your code here to invoke NO event
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();

	}

	public class myListAdapter extends ArrayAdapter<ProfileSlideData> {

		public myListAdapter() {
			super(MeTabActivity.this, R.layout.profileslidecustomlist, myCars);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			if (itemView == null) {
				itemView = getLayoutInflater().inflate(
						R.layout.profileslidecustomlist, parent, false);
			}
			ProfileSlideData currentCar = myCars.get(position);

			TextView tvItem = (TextView) itemView
					.findViewById(R.id.profilecustomItemName);

			tvItem.setText(currentCar.getItemname());
			return itemView;
		}
	}

}
