package com.kemchoslide;

public class Car {

	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getPhone() {
		return phone;
	}
	public void setPhone(String phone) {
		this.phone = phone;
	}
	public String getChatno() {
		return chatno;
	}
	public void setChatno(String chatno) {
		this.chatno = chatno;
	}
	public String getProfilepic() {
		return profilepic;
	}
	public void setProfilepic(String profilepic) {
		this.profilepic = profilepic;
	}
	public Car(String message, String phone, String chatno, String profilepic) {
		super();
		this.message = message;
		this.phone = phone;
		this.chatno = chatno;
		this.profilepic = profilepic;
	}
	String message;
	String phone;
	String chatno;
	String profilepic;
}
