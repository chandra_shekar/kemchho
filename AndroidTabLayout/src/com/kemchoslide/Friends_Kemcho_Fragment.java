package com.kemchoslide;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ImageloaderClass.ImageLoader;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class Friends_Kemcho_Fragment extends Fragment {
	private List<ContactsClass> myCars = new ArrayList<ContactsClass>();
	ArrayAdapter<ContactsClass> adapter;
	Context context;
	private static String url_create_product = " http://chat.scubeg.com/api/me/contacts/users";
	JSONParser jsonParser = new JSONParser();
	ArrayList<String> Id_no = new ArrayList<String>();
	ArrayList<String> name_d = new ArrayList<String>();
	ArrayList<String> phone_d = new ArrayList<String>();
	ArrayList<String> name_s = new ArrayList<String>();
	ArrayList<String> phone_s = new ArrayList<String>();

	ListView list;

	EditText edittext;
	int textlength = 0;
	ImageLoader imageLoader;

	View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.samsungcontacts, container, false);
		context = rootView.getContext();

		imageLoader = new ImageLoader(context);
		edittext = (EditText) rootView.findViewById(R.id.searchtext);
		list = (ListView) rootView.findViewById(R.id.getallcontacts);

		SwipeListViewTouchListener touchListener = new SwipeListViewTouchListener(
				list, new SwipeListViewTouchListener.OnSwipeCallback() {
					public void onSwipeLeft(ListView listView,
							int[] reverseSortedPositions) {

						String PhoneNo = "tel:"
								+ phone_s.get(reverseSortedPositions[0]);
						Intent i = new Intent(Intent.ACTION_CALL);
						i.setData(Uri.parse(PhoneNo));
						startActivity(i);

					}

					public void onSwipeRight(ListView listView,
							int[] reverseSortedPositions) {

						Log.i(this.getClass().getName(), "swipe right : pos="
								+ reverseSortedPositions[0]);
						String uri = "smsto:"
								+ phone_s.get(reverseSortedPositions[0]);
						Intent intent = new Intent(Intent.ACTION_SENDTO,
								Uri.parse(uri));
						// intent.putExtra("sms_body", "hi");
						intent.putExtra("compose_mode", true);
						startActivity(intent);

					}
				}, true, // example : left action = dismiss
				false);

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.GINGERBREAD) {
			// only for gingerbread and newer versions

			list.setOnTouchListener(touchListener);
			// Setting this scroll listener is required to ensure that during
			// ListView scrolling,
			// we don't look for swipes.
			list.setOnScrollListener(touchListener.makeScrollListener());
		}

		edittext.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				myCars.removeAll(myCars);
				textlength = edittext.getText().length();
				name_s.clear();
				phone_s.clear();
				for (int nn = 0; nn < name_d.size(); nn++) {
					if (textlength <= name_d.get(nn).length()) {
						if (name_d
								.get(nn)
								.toUpperCase()
								.contains(
										edittext.getText().toString()
												.toUpperCase().trim())) {
							name_s.add(name_d.get(nn));
							phone_s.add(phone_d.get(nn));
							myCars.add(new ContactsClass(name_d.get(nn),
									phone_d.get(nn)));
						}
					}
				}
				ListView list = (ListView) rootView
						.findViewById(R.id.getallcontacts);

				list.setAdapter(adapter);
			}
		});

		new Add_member().execute();

		return rootView;
	}

	private class myListAdapter extends ArrayAdapter<ContactsClass> {

		public myListAdapter() {
			super(context, R.layout.contactcustom, myCars);
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View itemView = convertView;
			if (itemView == null) {
				itemView = getLayoutInflater(null).inflate(
						R.layout.contactcustom, parent, false);
			}
			ContactsClass currentCar = myCars.get(position);

			TextView textview = (TextView) itemView
					.findViewById(R.id.textView1);
			TextView phone = (TextView) itemView.findViewById(R.id.textView2);
			textview.setText(currentCar.getName());
			phone.setText(currentCar.getPhone());

			ImageView addmember = (ImageView) itemView
					.findViewById(R.id.ivinvitenewmember);
			ImageView imageprofile = (ImageView) itemView
					.findViewById(R.id.imageView1);

			imageLoader.DisplayImage(
					"http://chat.scubeg.com" + currentCar.getprofile_pic(),
					imageprofile);

			addmember.setVisibility(itemView.GONE);

			return itemView;
		}

	}

	class Add_member extends AsyncTask<String, String, String> {
		protected String doInBackground(String... args) {
			SharedPreferences setting = context
					.getSharedPreferences("login", 0);
			String authkey = setting.getString("authkey", "nothing");
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("_ses", authkey));
			JSONObject json = jsonParser.makeHttpRequest(url_create_product,
					"GET", params);

			try {
				json.getString("status");
				String data = json.getString("data");

				JSONObject user = new JSONObject(data);
				String userdata = user.getString("users");
				JSONArray responcearray = new JSONArray(userdata);

				for (int k = 0; k < responcearray.length(); k++) {
					JSONObject jObject = responcearray.getJSONObject(k);
					String idno = jObject.getString("id");
					String first_name = jObject.getString("first_name");
					String last_name = jObject.getString("last_name");
					String phone = jObject.getString("phone");
					String profilepic = jObject.getString("profile_pic");
					String name = first_name + "  " + last_name;

					Id_no.add(idno);
					myCars.add(new ContactsClass(name, phone, idno, profilepic));
					name_d.add(name);
					phone_d.add(phone);
					name_s.add(name);
					phone_s.add(phone);

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			list = (ListView) rootView.findViewById(R.id.getallcontacts);
			adapter = new myListAdapter();
			list.setAdapter(adapter);

		}
	}

}
