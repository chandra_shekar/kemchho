package com.kemchoslide;

public class ContactsClass {

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getprofile_pic() {
		return profile_pic;
	}

	public void setprofile_pic(String profile_pic) {
		this.profile_pic = profile_pic;
	}

	/**
	 * @param name
	 * @param phone
	 */
	public ContactsClass(String name, String phone) {
		super();
		this.name = name;
		this.phone = phone;
	}

	public String getids() {
		return ids;
	}

	public void setids(String id) {
		this.ids = id;
	}

	public ContactsClass(String name, String phone, String ids,
			String profile_pic) {
		super();
		this.name = name;
		this.phone = phone;
		this.ids = ids;
		this.profile_pic = profile_pic;
	}

	String name, phone, ids, profile_pic;

}
