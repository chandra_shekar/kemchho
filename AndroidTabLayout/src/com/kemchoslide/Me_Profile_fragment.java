package com.kemchoslide;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import ImageloaderClass.ImageLoader;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class Me_Profile_fragment extends Fragment {

	private Uri mImageCaptureUri;
	private ImageView mImageView;
	private AlertDialog dialog;

	Bitmap bitmapOrg;
	ImageLoader imageLoader;
	InputStream is;
	private static final int PICK_FROM_CAMERA = 1;
	private static final int CROP_FROM_CAMERA = 2;
	private static final int PICK_FROM_FILE = 3;
	View rootView;
	Context context;

	// Location

	GPSTracker gps;

	TextView locationtextview;
	double latitude;
	double longitude;

	// /

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootView = inflater.inflate(R.layout.profilephotowithcrop, container,
				false);
		context = rootView.getContext();

		// /////////////////////////////Location
		locationtextview=(TextView)rootView.findViewById(R.id.tv_location);
		gps = new GPSTracker(context);

		// check if GPS enabled
		if (gps.canGetLocation()) {

			latitude = gps.getLatitude();
			longitude = gps.getLongitude();

			// \n is for new line
			Toast.makeText(
					context,
					"Your Location is - \nLat: " + latitude + "\nLong: "
							+ longitude, Toast.LENGTH_LONG).show();
		} else {
			// can't get location
			// GPS or Network is not enabled
			// Ask user to enable GPS/network in settings
			gps.showSettingsAlert();
		}

		// //////////
		Geocoder geocoder;

		List<Address> addresses = null;
		geocoder = new Geocoder(context, Locale.getDefault());
		try {
			addresses = geocoder.getFromLocation(latitude, longitude, 1);
		} catch (IOException e) {
			e.printStackTrace();
		}

		String address = addresses.get(0).getAddressLine(0);
		String city = addresses.get(0).getAddressLine(1);
		String country = addresses.get(0).getAddressLine(2);

		Toast.makeText(context, "" + address + " " + city + " " + country,
				Toast.LENGTH_SHORT).show();

		locationtextview.setText(address + "\n" + city);

		// //////////////////////////

		captureImageInitialization();

		imageLoader = new ImageLoader(context);

		bitmapOrg = BitmapFactory.decodeResource(getResources(),
				R.drawable.logo_title);
		// Button button = (Button) findViewById(R.id.SelectImageBtn);
		mImageView = (ImageView) rootView.findViewById(R.id.photoimage);

		mImageView.setImageBitmap(BitmapFactory
				.decodeFile("/storage/sdcard0/DCIM/me.jpg"));

		// button.setOnClickListener(new View.OnClickListener() {
		// public void onClick(View v) {
		// dialog.show();
		// }
		// });
		mImageView.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				dialog.show();
			}
		});

		final Button male = (Button) rootView.findViewById(R.id.maleButton);
		final Button female = (Button) rootView.findViewById(R.id.femaleButton);

		male.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				male.setBackgroundResource(R.drawable.btn_info_male_on);
				female.setBackgroundResource(R.drawable.btn_info_female_off);
			}
		});
		female.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				male.setBackgroundResource(R.drawable.btn_info_male_off);
				female.setBackgroundResource(R.drawable.btn_info_female_on);
			}
		});

		return rootView;
	}

	private void captureImageInitialization() {
		/**
		 * a selector dialog to display two image source options, from camera
		 * �Take from camera� and from existing files �Select from gallery�
		 */
		final String[] items = new String[] { "Take from camera",
				"Select from gallery" };
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
				android.R.layout.select_dialog_item, items);
		AlertDialog.Builder builder = new AlertDialog.Builder(context);

		builder.setTitle("Select Image");
		builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int item) { // pick from
																	// camera
				if (item == 0) {
					/**
					 * To take a photo from camera, pass intent action
					 * �MediaStore.ACTION_IMAGE_CAPTURE� to open the camera app.
					 */
					Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

					/**
					 * Also specify the Uri to save the image on specified path
					 * and file name. Note that this Uri variable also used by
					 * gallery app to hold the selected image path.
					 */
					mImageCaptureUri = Uri.fromFile(new File(Environment
							.getExternalStorageDirectory(), "tmp_avatar_"
							+ String.valueOf(System.currentTimeMillis())
							+ ".jpg"));

					intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT,
							mImageCaptureUri);

					try {
						intent.putExtra("return-data", true);

						startActivityForResult(intent, PICK_FROM_CAMERA);
					} catch (ActivityNotFoundException e) {
						e.printStackTrace();
					}
				} else {
					// pick from file
					/**
					 * To select an image from existing files, use
					 * Intent.createChooser to open image chooser. Android will
					 * automatically display a list of supported applications,
					 * such as image gallery or file manager.
					 */
					Intent intent = new Intent();

					intent.setType("image/*");
					intent.setAction(Intent.ACTION_GET_CONTENT);

					startActivityForResult(Intent.createChooser(intent,
							"Complete action using"), PICK_FROM_FILE);
				}
			}
		});

		dialog = builder.create();
	}

	public class CropOptionAdapter extends ArrayAdapter<CropOption> {
		private ArrayList<CropOption> mOptions;
		private LayoutInflater mInflater;

		public CropOptionAdapter(Context context, ArrayList<CropOption> options) {
			super(context, R.layout.crop_selector, options);

			mOptions = options;

			mInflater = LayoutInflater.from(context);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup group) {
			if (convertView == null)
				convertView = mInflater.inflate(R.layout.crop_selector, null);

			CropOption item = mOptions.get(position);

			if (item != null) {
				((ImageView) convertView.findViewById(R.id.iv_icon))
						.setImageDrawable(item.icon);
				((TextView) convertView.findViewById(R.id.tv_name))
						.setText(item.title);

				return convertView;
			}

			return null;
		}
	}

	public class CropOption {
		public CharSequence title;
		public Drawable icon;
		public Intent appIntent;
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		switch (requestCode) {
		case PICK_FROM_CAMERA:
			/**
			 * After taking a picture, do the crop
			 */
			doCrop();
			break;

		case PICK_FROM_FILE:
			/**
			 * After selecting image from files, save the selected path
			 */
			mImageCaptureUri = data.getData();

			doCrop();

			break;

		case CROP_FROM_CAMERA:
			Bundle extras = data.getExtras();
			/**
			 * After cropping the image, get the bitmap of the cropped image and
			 * display it on imageview.
			 */
			if (extras != null) {
				Bitmap photo = extras.getParcelable("data");

				mImageView.setImageBitmap(photo);
				bitmapOrg = photo;

				save("me");
				UploadPhoto();
				Toast.makeText(context, "Thanks For Uploading Photo!",
						Toast.LENGTH_SHORT).show();
				imageLoader.clearCache();
			}
			File f = new File(mImageCaptureUri.getPath());
			/**
			 * Delete the temporary image
			 */
			if (f.exists())
				f.delete();
			break;
		}
	}

	private void doCrop() {
		final ArrayList<CropOption> cropOptions = new ArrayList<CropOption>();
		/**
		 * Open image crop app by starting an intent
		 * �com.android.camera.action.CROP�.
		 */
		Intent intent = new Intent("com.android.camera.action.CROP");
		intent.setType("image/*");

		/**
		 * Check if there is image cropper app installed.
		 */
		List<ResolveInfo> list = context.getPackageManager()
				.queryIntentActivities(intent, 0);

		int size = list.size();

		/**
		 * If there is no image cropper app, display warning message
		 */
		if (size == 0) {

			Toast.makeText(context, "Can not find image crop app",
					Toast.LENGTH_SHORT).show();

			return;
		} else {
			/**
			 * Specify the image path, crop dimension and scale
			 */
			intent.setData(mImageCaptureUri);

			intent.putExtra("outputX", 200);
			intent.putExtra("outputY", 200);
			intent.putExtra("aspectX", 1);
			intent.putExtra("aspectY", 1);
			intent.putExtra("scale", true);
			intent.putExtra("return-data", true);
			/**
			 * There is posibility when more than one image cropper app exist,
			 * so we have to check for it first. If there is only one app, open
			 * then app.
			 */
			if (size == 1) {
				Intent i = new Intent(intent);
				ResolveInfo res = list.get(0);

				i.setComponent(new ComponentName(res.activityInfo.packageName,
						res.activityInfo.name));

				startActivityForResult(i, CROP_FROM_CAMERA);
			} else {
				/**
				 * If there are several app exist, create a custom chooser to
				 * let user selects the app.
				 */
				for (ResolveInfo res : list) {
					final CropOption co = new CropOption();

					co.title = context.getPackageManager().getApplicationLabel(
							res.activityInfo.applicationInfo);
					co.icon = context.getPackageManager().getApplicationIcon(
							res.activityInfo.applicationInfo);
					co.appIntent = new Intent(intent);
					co.appIntent
							.setComponent(new ComponentName(
									res.activityInfo.packageName,
									res.activityInfo.name));

					cropOptions.add(co);
				}

				CropOptionAdapter adapter = new CropOptionAdapter(context,
						cropOptions);

				AlertDialog.Builder builder = new AlertDialog.Builder(context);
				builder.setTitle("Choose Crop App");
				builder.setAdapter(adapter,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int item) {
								startActivityForResult(
										cropOptions.get(item).appIntent,
										CROP_FROM_CAMERA);
							}
						});

				builder.setOnCancelListener(new DialogInterface.OnCancelListener() {
					public void onCancel(DialogInterface dialog) {

						if (mImageCaptureUri != null) {
							context.getContentResolver().delete(
									mImageCaptureUri, null, null);
							mImageCaptureUri = null;
						}
					}
				});

				AlertDialog alert = builder.create();

				alert.show();
			}
		}
	}

	private void UploadPhoto() {
		ByteArrayOutputStream bao = new ByteArrayOutputStream();
		bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 90, bao);
		byte[] ba = bao.toByteArray();
		String ba1 = Base64.encodeBytes(ba);
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();

		SharedPreferences setting = context.getSharedPreferences("login", 0);
		String authkey = setting.getString("authkey", "nothing");

		nameValuePairs.add(new BasicNameValuePair("_action",
				"upload_profile_image"));
		nameValuePairs.add(new BasicNameValuePair("image", ba1));
		nameValuePairs.add(new BasicNameValuePair("_ses", authkey));

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost("http://chat.scubeg.com");
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			is = entity.getContent();
		} catch (Exception e) {
			Log.e("log_tag", "Error in http connection " + e.toString());

		}

	}

	public void save(String filename) {
		// String filename;
		// Date date = new Date(0);
		// SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
		// filename = sdf.format(date);

		try {

			String path = Environment.getExternalStorageDirectory().toString();
			FileOutputStream fOut = null;
			File file = new File(path, "/DCIM/" + filename + ".jpg");
			fOut = new FileOutputStream(file);

			bitmapOrg.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
			fOut.flush();
			fOut.close();

			MediaStore.Images.Media.insertImage(context.getContentResolver(),
					file.getAbsolutePath(), file.getName(), file.getName());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
