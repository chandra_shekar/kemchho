package com.kemchoslide;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ImageloaderClass.ImageLoader;
import android.app.Activity;
import android.content.SharedPreferences;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class NewChatView extends Activity implements OnClickListener {
	// location
	GPSTracker gps;

	TextView locationtextview;
	double latitude;
	double longitude;

	// /
	EditText etsendsms;
	ListView list;
	ArrayAdapter<Car> adapter;
	private List<Car> myCars = new ArrayList<Car>();

	String groupid;
	private String url_create_product = "http://chat.scubeg.com/api/chats/";
	private String urlmessagePost = "http://chat.scubeg.com";
	JSONParser jsonParser = new JSONParser();

	String password;
	String authkey;
	int distFromTop;
	int firstVisiblePosition;

	Thread timer;
	boolean running = false;
	Button btmap, btsend;
	ImageLoader imageLoader;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);

		setContentView(R.layout.chatview);
		btsend = (Button) findViewById(R.id.btsend);
		btsend.setOnClickListener(this);

		btmap = (Button) findViewById(R.id.btmap);
		btmap.setOnClickListener(this);
		etsendsms = (EditText) findViewById(R.id.etsend);
		imageLoader = new ImageLoader(this.getApplicationContext());

		locationtextview =(TextView)findViewById(R.id.tvlocation);
		// imageLoader.clearCache();
		// clearApplicationData();

		etsendsms.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				btmap.setVisibility(View.GONE);
				btsend.setBackgroundResource(R.drawable.btsend);
			}
		});

		SharedPreferences setting = getSharedPreferences("login", 0);
		authkey = setting.getString("authkey", "nothing");
		etsendsms = (EditText) findViewById(R.id.etsend);

		Button btsend = (Button) findViewById(R.id.btsend);
		btsend.setOnClickListener(this);

		String newid = getIntent().getStringExtra("groupid");
		groupid = getIntent().getStringExtra("groupid");
		url_create_product += newid;
		Log.i("group id", "" + groupid);

		Toast.makeText(getBaseContext(), "" + newid, Toast.LENGTH_LONG).show();

		populateListview();
		new CreateNewProduct().execute();

		timer = new Thread() {
			public void run() {
				running = true;

				while (running) {
					try {
						sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						new CreateNewProduct().execute();
					}
				}
			}
		};

		timer.start();
		list = (ListView) findViewById(R.id.chatlist);

		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int pos, long arg3) {
				Car currentCar = myCars.get(pos);

				Log.i("Long Presssed", "Long Pressed");
				new DeleteConversation().execute("" + currentCar.getChatno());

				return true;
			}
		});

	}

	// @Override
	// public boolean onCreateOptionsMenu(Menu menu) {
	// getMenuInflater().inflate(R.menu.menucool, menu);
	// return true;
	//
	// }

	// @Override
	// public boolean onOptionsItemSelected(MenuItem item) {
	//
	// switch (item.getItemId()) {
	// case R.id.deleteallchats:
	//
	// myCars.remove(myCars);
	//
	// new Delete_All_Message().execute();
	// break;
	// }
	// return true;
	//
	// }

	class CreateNewProduct extends AsyncTask<String, String, String> {

		JSONArray contacts = null;

		protected String doInBackground(String... args) {

			SharedPreferences setting = getSharedPreferences("login", 0);
			String authkey = setting.getString("authkey", "nothing");
			String status = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("_ses", "" + authkey));

			JSONObject json = jsonParser.makeHttpRequest(url_create_product,
					"GET", params);
			try {

				status = json.getString("status");
				String recent_chats = json.getString("chats");

				JSONArray responcearray = new JSONArray(recent_chats);

				myCars.removeAll(myCars);

				for (int i = 0; i < responcearray.length(); i++) {
					JSONObject jObject = responcearray.getJSONObject(i);
					String message = jObject.getString("message");
					String phone = jObject.getString("phone");
					String chatno = jObject.getString("id");
					String profile_pic = jObject.getString("profile_pic");
					Log.i("Profile pic Url", "" + profile_pic);
					// public Car(String message, String phone, String chatno,
					// String profilepic) {
					myCars.add(new Car(message, phone, chatno,
							"http://chat.scubeg.com" + profile_pic));
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			populateListview();

		}
	}

	public void populateListview() {
		adapter = new myListAdapter();
		list = (ListView) findViewById(R.id.chatlist);
		list.setAdapter(adapter);

	}

	public class myListAdapter extends ArrayAdapter<Car> {

		public myListAdapter() {
			super(NewChatView.this, R.layout.test, myCars);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View itemView = convertView;
			if (itemView == null) {
				itemView = getLayoutInflater().inflate(R.layout.test, parent,
						false);
			}
			Car currentCar = myCars.get(position);
			TextView tvright = (TextView) itemView.findViewById(R.id.tvRight);
			TextView tvleft = (TextView) itemView.findViewById(R.id.tvLeft);
			ImageView IVright = (ImageView) itemView.findViewById(R.id.ivright);
			ImageView IVleft = (ImageView) itemView.findViewById(R.id.ivleft);

			SharedPreferences setting = getSharedPreferences("userinfo", 0);
			String userphone = setting.getString("phone", "nothing");

			if (currentCar.getPhone().equals(userphone)) {
				tvright.setText(currentCar.getMessage());
				IVleft.setVisibility(itemView.GONE);
				tvleft.setVisibility(itemView.GONE);
				imageLoader.DisplayImage(currentCar.getProfilepic(), IVright);
			} else {
				tvleft.setText(currentCar.getMessage());
				IVright.setVisibility(itemView.GONE);
				tvright.setVisibility(itemView.GONE);
				imageLoader.DisplayImage(currentCar.getProfilepic(), IVleft);
			}

			return itemView;
		}
	}

	class Sendmessage extends AsyncTask<String, String, String> {
		protected String doInBackground(String... args) {

			Log.i("authkey after loging", "" + authkey);
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("_action", "addchat"));
			params.add(new BasicNameValuePair("group_id", groupid));
			params.add(new BasicNameValuePair("message", args[0]));
			params.add(new BasicNameValuePair("_ses", authkey));
			JSONObject json = jsonParser.makeHttpRequest(urlmessagePost,
					"POST", params);
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		}
	}

	class Delete_All_Message extends AsyncTask<String, String, String> {

		protected String doInBackground(String... args) {

			Log.i("authkey after loging", "" + authkey);
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("_action", "clear_messages"));
			params.add(new BasicNameValuePair("_ses", authkey));
			params.add(new BasicNameValuePair("group_id", groupid));

			JSONObject json = jsonParser.makeHttpRequest(urlmessagePost,
					"POST", params);

			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
		}
	}

	public void onClick(View v) {
		// if (etsendsms.getText().toString().length() != 0)
		String newmessagetosend = etsendsms.getText().toString();
		etsendsms.setText("");

		if (newmessagetosend.length() != 0) {

			gps = new GPSTracker(NewChatView.this);

			// check if GPS enabled
			if (gps.canGetLocation()) {

				latitude = gps.getLatitude();
				longitude = gps.getLongitude();

				// \n is for new line
				Toast.makeText(
						getApplicationContext(),
						"Your Location is - \nLat: " + latitude + "\nLong: "
								+ longitude, Toast.LENGTH_LONG).show();
			} else {
				// can't get location
				// GPS or Network is not enabled
				// Ask user to enable GPS/network in settings
				gps.showSettingsAlert();
			}

			// //////////
			Geocoder geocoder;
			
			List<Address> addresses = null;
			geocoder = new Geocoder(NewChatView.this, Locale.getDefault());
			try {
				addresses = geocoder.getFromLocation(latitude, longitude, 1);
			} catch (IOException e) {
				e.printStackTrace();
			}

			String address = addresses.get(0).getAddressLine(0);
			String city = addresses.get(0).getAddressLine(1);
			String country = addresses.get(0).getAddressLine(2);

			Toast.makeText(getBaseContext(),
					"" + address + " " + city + " " + country,
					Toast.LENGTH_SHORT).show();

			locationtextview.setText(address+" "+city);
			new Sendmessage().execute(newmessagetosend);

		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		running = false;
	}

	class DeleteConversation extends AsyncTask<String, String, String> {

		protected String doInBackground(String... args) {

			SharedPreferences setting = getSharedPreferences("login", 0);
			authkey = setting.getString("authkey", "nothing");
			String status = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("_action", "delete_message"));
			params.add(new BasicNameValuePair("chat_id", args[0]));
			params.add(new BasicNameValuePair("_ses", authkey));
			// {'_action':'delete_message','chat_id':<id>}
			JSONObject json = jsonParser.makeHttpRequest(
					"http://chat.scubeg.com", "POST", params);
			return args[0];
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Toast.makeText(getApplicationContext(),
					"deleted  in chat  " + result, Toast.LENGTH_SHORT).show();
		}
	}

	public void clearApplicationData() {
		File cache = getCacheDir();
		File appDir = new File(cache.getParent());
		if (appDir.exists()) {
			String[] children = appDir.list();
			for (String s : children) {
				if (!s.equals("lib")) {
					deleteDir(new File(appDir, s));
					Log.i("TAG",
							"**************** File /data/data/APP_PACKAGE/" + s
									+ " DELETED *******************");
				}
			}
		}
	}

	public static boolean deleteDir(File dir) {
		if (dir != null && dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

}
