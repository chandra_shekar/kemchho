package com.kemchoslide;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.telephony.gsm.SmsManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Friends_Mobile_Fragment extends Fragment {

	int i = 0;
	Context context;
	boolean firsttime;
	ListView list;
	EditText edittext;
	int textlength = 0;
	String Phone_no_to_send_sms = null;
	private List<ContactsClass> myCars = new ArrayList<ContactsClass>();
	ArrayList<String> Names_withnull = new ArrayList<String>();
	ArrayList<String> Phones_withnull = new ArrayList<String>();
	ArrayList<String> real_name = new ArrayList<String>();
	ArrayList<String> real_phone = new ArrayList<String>();
	ArrayList<String> Shorted_name = new ArrayList<String>();
	ArrayList<String> Shorted_phone = new ArrayList<String>();
	ArrayAdapter<ContactsClass> adapter;
	Animation animBounce;
	private static String url_create_product = "";
	JSONParser jsonParser = new JSONParser();
	// UpdateContacts updatecontact;
	Domain domain;

	ImageView All_contact;

	View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.samsungcontacts, container, false);

		context = rootView.getContext();

		domain = new Domain();
		url_create_product = domain.getMainDomain(0);

		// All_contact=(ImageView)findViewById(R.id.menu_user);
		// All_contact.setOnClickListener(this);

		SharedPreferences setting = context.getSharedPreferences("contact", 0);
		firsttime = setting.getBoolean("first", true);
		edittext = (EditText) rootView.findViewById(R.id.searchtext);
		ListView list = (ListView) rootView.findViewById(R.id.getallcontacts);
		// list.setAdapter(adapter);

		animBounce = AnimationUtils.loadAnimation(context, R.anim.rotate);
		if (firsttime) {
			new contacts().execute();
			firsttime = false;
			SharedPreferences firsttimecontact = context.getSharedPreferences(
					"contact", 0);
			SharedPreferences.Editor loading = firsttimecontact.edit();
			loading.putBoolean("first", firsttime);
			loading.commit();

		} else {

			// updatecontact = new UpdateContacts(this);

			// updatecontact.runcontacts();

			// Intent service = new Intent(ContactsSamsung.this,
			// MyService.class);
			// startService(service);
			real_name = loadArray("name", context);
			real_phone = loadArray("phone", context);
			for (int ln = 0; ln < real_name.size(); ln++) {
				myCars.add(new ContactsClass(real_name.get(ln), real_phone
						.get(ln)));
			}
			adapter = new myListAdapter();

			list.setAdapter(adapter);

			for (int ln = 0; ln < real_name.size(); ln++) {
				Shorted_name.add(real_name.get(ln));
				Shorted_phone.add(real_phone.get(ln));
				myCars.add(new ContactsClass(real_name.get(ln), real_phone
						.get(ln)));
			}
		}

		SwipeListViewTouchListener touchListener = new SwipeListViewTouchListener(
				list, new SwipeListViewTouchListener.OnSwipeCallback() {
					public void onSwipeLeft(ListView listView,
							int[] reverseSortedPositions) {

						String PhoneNo = "tel:"
								+ Shorted_phone.get(reverseSortedPositions[0]);
						Intent i = new Intent(Intent.ACTION_CALL);
						i.setData(Uri.parse(PhoneNo));
						startActivity(i);

					}

					public void onSwipeRight(ListView listView,
							int[] reverseSortedPositions) {

						Log.i(this.getClass().getName(), "swipe right : pos="
								+ reverseSortedPositions[0]);
						String uri = "smsto:"
								+ Shorted_phone.get(reverseSortedPositions[0]);
						Intent intent = new Intent(Intent.ACTION_SENDTO,
								Uri.parse(uri));
						// intent.putExtra("sms_body", "hi");
						intent.putExtra("compose_mode", true);
						startActivity(intent);

					}
				}, true, // example : left action = dismiss
				false);

		// if (android.os.Build.VERSION.SDK_INT >=
		// android.os.Build.VERSION_CODES.GINGERBREAD) {
		// // only for gingerbread and newer versions
		//
		list.setOnTouchListener(touchListener);
		// // Setting this scroll listener is required to ensure that during
		// // ListView scrolling,
		// // we don't look for swipes.
		list.setOnScrollListener(touchListener.makeScrollListener());
		// }

		edittext.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged1(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				myCars.removeAll(myCars);
				textlength = edittext.getText().length();
				Shorted_name.clear();
				Shorted_phone.clear();
				for (int nn = 0; nn < real_name.size(); nn++) {
					if (textlength <= real_name.get(nn).length()) {
						if (real_name
								.get(nn)
								.toUpperCase()
								.contains(
										edittext.getText().toString()
												.toUpperCase().trim())) {
							Shorted_name.add(real_name.get(nn));
							Shorted_phone.add(real_phone.get(nn));
							Log.i("shorted", "" + real_name.get(nn));
							myCars.add(new ContactsClass(real_name.get(nn),
									real_phone.get(nn)));
						}
					}
				}
				ListView list = (ListView) rootView
						.findViewById(R.id.getallcontacts);

				list.setAdapter(adapter);
			}

			@Override
			public void afterTextChanged(Editable s) {

			}
		});

		return rootView;
	}

	public class contacts extends AsyncTask<String, String, String> {
		ImageView loading;
		Dialog dialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			dialog = new Dialog(context);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			dialog.setContentView(R.layout.lodingicon);
			loading = (ImageView) dialog.findViewById(R.id.loadingspinner);
			animBounce.setAnimationListener(null);
			loading.startAnimation(animBounce);
			dialog.setCancelable(false);
			dialog.show();
		}

		protected String doInBackground(String... args) {
			ContentResolver cr = context.getContentResolver();
			Cursor cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null,
					null, null, null);

			if (cur.getCount() > 0) {
				i = 0;
				while (cur.moveToNext()) {
					String id = cur.getString(cur
							.getColumnIndex(ContactsContract.Contacts._ID));
					String name = cur
							.getString(cur
									.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
					if (Integer
							.parseInt(cur.getString(cur
									.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0) {
						Cursor pCur = cr
								.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
										null,
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID
												+ " = ?", new String[] { id },
										null);

						while (pCur.moveToNext()) {
							String phoneNo = pCur
									.getString(pCur
											.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
							Names_withnull.add(name);
							Phones_withnull.add(phoneNo);

						}
						pCur.close();
					}
				}
			}
			return null;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			animBounce.cancel();
			dialog.dismiss();
			for (int k = 0; k < Names_withnull.size(); k++) {
				String value = Names_withnull.get(k);
				if (value != null) {
					real_name.add(Names_withnull.get(k));
					real_phone.add(Phones_withnull.get(k));
					Shorted_name.add(Names_withnull.get(k));
					Shorted_phone.add(Phones_withnull.get(k));
					Log.i("Without null", "" + Names_withnull.get(k));
					myCars.add(new ContactsClass(Names_withnull.get(k),
							Phones_withnull.get(k)));
				}
			}
			ListView list = (ListView) rootView
					.findViewById(R.id.getallcontacts);

			
			adapter = new myListAdapter();
			 list.setAdapter(adapter);
			saveArray(real_name, "name", context);
			saveArray(real_phone, "phone", context);
		}
	}

	private class myListAdapter extends ArrayAdapter<ContactsClass> {

		public myListAdapter() {
			super(context, R.layout.contactcustom, myCars);
		}

		@Override
		public View getView(final int position, View convertView,
				ViewGroup parent) {
			View itemView = convertView;
			if (itemView == null) {

				LayoutInflater inflater = (LayoutInflater) rootView
						.getContext().getSystemService(
								Context.LAYOUT_INFLATER_SERVICE);

				itemView = inflater.inflate(R.layout.contactcustom, null);

				// itemView = getLayoutInflater(null).inflate(
				// R.layout.contactcustom, parent, false);

			}
			ContactsClass currentCar = myCars.get(position);

			TextView textview = (TextView) itemView
					.findViewById(R.id.textView1);
			TextView phone = (TextView) itemView.findViewById(R.id.textView2);
			textview.setText(currentCar.getName());
			phone.setText(currentCar.getPhone());

			final ImageView addmember = (ImageView) itemView
					.findViewById(R.id.ivinvitenewmember);

			addmember.setOnClickListener(new OnClickListener() {

				public void onClick(View v) {

					// addmember.setVisibility(itemView.GONE);

					String numb = Shorted_phone.get(position);

					String text = numb.replaceAll("\\s", "");

					String numbers = text.substring(Math.max(0,
							text.length() - 10));
					Log.i("numbers", "" + numbers);
					Toast.makeText(context, "" + numbers, Toast.LENGTH_SHORT)
							.show();
					new Add_member().execute(Shorted_phone.get(position));

				}
			});

			return itemView;
		}

	}

	class Add_member extends AsyncTask<String, String, String> {

		JSONArray contacts = null;

		protected String doInBackground(String... args) {

			SharedPreferences setting = context
					.getSharedPreferences("login", 0);

			String authkey = setting.getString("authkey", "nothing");

			String status = null;

			String numbertosend = args[0].replaceAll("\\s+", "");

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("_action", "addchat"));
			params.add(new BasicNameValuePair("to", numbertosend));
			params.add(new BasicNameValuePair("message", "Add Me as friend"));
			params.add(new BasicNameValuePair("_ses", authkey));

			Phone_no_to_send_sms = numbertosend;
			JSONObject json = jsonParser.makeHttpRequest(url_create_product,
					"POST", params);
			try {
				status = json.getString("status");
				Log.i("new status", "" + status);
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);

			if (result.equals("failure")) {
				showAlertDialog1(
						Phone_no_to_send_sms,
						context,
						"Send SMS",
						"Your Friend is not yet part of Kem Chho would you like Invite",
						false);
			}

		}
	}

	@SuppressWarnings("deprecation")
	public void showAlertDialog1(final String phone, final Context context,
			String title, String message, Boolean status) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setIcon((status) ? R.drawable.ic_launcher
				: R.drawable.ic_launcher);
		alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				try {

					String sms = "Guys Check this Out http://sundaybazaar.in/Kem_Chho.apk   ";
					SmsManager smsManager = SmsManager.getDefault();
					smsManager.sendTextMessage(phone, null, sms, null, null);
					Toast.makeText(context, "SMS Sent! more" + "",
							Toast.LENGTH_LONG).show();

				} catch (Exception e) {
					Toast.makeText(context,
							"SMS faild, please try again later!  " + phone,
							Toast.LENGTH_LONG).show();
					e.printStackTrace();
				}
			}
		});
		alertDialog.show();
	}

	public boolean saveArray(ArrayList<String> names2, String arrayName,
			Context mContext) {
		SharedPreferences prefs = mContext.getSharedPreferences(
				"preferencename", 0);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putInt(arrayName + "_size", names2.size());
		for (int i = 0; i < names2.size(); i++)
			editor.putString(arrayName + "_" + i, names2.get(i));
		return editor.commit();
	}

	public ArrayList<String> loadArray(String arrayName, Context mContext) {
		SharedPreferences prefs = mContext.getSharedPreferences(
				"preferencename", 0);
		int size = prefs.getInt(arrayName + "_size", 0);
		ArrayList<String> array = new ArrayList<String>();
		for (int i = 0; i < size; i++)
			// array.get(i) = prefs.getString(arrayName + "_" + i, null);
			array.add(prefs.getString(arrayName + "_" + i, null));
		return array;
	}

	class Update extends AsyncTask<String, String, String> {

		JSONArray contacts = null;

		protected String doInBackground(String... args) {

			return null;
		}

	}

	// public void onClick(View v) {
	// switch (v.getId()) {
	// case R.id.menu_user:
	//
	// Intent all_contact_tab = new Intent(ContactsSamsung.this,
	// TabSample.class);
	// startActivity(all_contact_tab);
	// break;
	//
	// }
	// }

}