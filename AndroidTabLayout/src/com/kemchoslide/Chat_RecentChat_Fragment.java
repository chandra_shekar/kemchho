package com.kemchoslide;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import ImageloaderClass.ImageLoader;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Chat_RecentChat_Fragment extends Fragment implements
		OnClickListener {

	String authkey;
	EditText edittext;
	int textlength = 0;
	Thread timer;
	boolean running = false;
	private static String url_recentchat = "";
	private static String url_delete_conversation = "";
	JSONParser jsonParser = new JSONParser();
	String group_id[] = new String[20];
	Context context;
	ArrayList<String> text_sort = new ArrayList<String>();
	ArrayList<String> image_sort = new ArrayList<String>();
	ArrayList<String> User_photo = new ArrayList<String>();
	ArrayList<String> allname = new ArrayList<String>();
	ArrayList<String> allphone = new ArrayList<String>();
	ArrayList<String> allimage = new ArrayList<String>();

	ImageLoader imageLoader;
	Domain domain;
	View rootView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.chat, container, false);
		context = rootView.getContext();

		// Intent notification = new Intent(context, NotificationService.class);
		// startService(notification);

		domain = new Domain();
		url_recentchat = domain.getrecentchat(0);
		url_delete_conversation = domain.delete_conversation(0);
		imageLoader = new ImageLoader(this.context.getApplicationContext());

		edittext = (EditText) rootView.findViewById(R.id.searchtext);
		// ImageView Logout = (ImageView)rootView.
		// findViewById(R.id.menuViewHomeButton);
		// Logout.setOnClickListener(this);

		// ImageView All_contact = (ImageView) findViewById(R.id.menu_user);
		// All_contact.setOnClickListener(this);

		InputMethodManager imm = (InputMethodManager) context
				.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(edittext.getWindowToken(), 0);
		ListView list = (ListView) rootView.findViewById(R.id.getallcontacts);

		list.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {
				 Intent chatview = new Intent(context, NewChatView.class);
				 chatview.putExtra("groupid", group_id[pos]);
				 startActivity(chatview);
			}
		});

		list.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				new DeleteConversation().execute(group_id[arg2]);
				Toast.makeText(context, "" + group_id[arg2], Toast.LENGTH_SHORT)
						.show();
				return true;
			}
		});

		edittext.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				textlength = edittext.getText().length();
				text_sort.clear();
				image_sort.clear();
				User_photo.clear();
				for (int nn = 0; nn < allname.size(); nn++) {
					if (textlength <= allname.get(nn).length()) {
						if (allname
								.get(nn)
								.toUpperCase()
								.contains(
										edittext.getText().toString()
												.toUpperCase().trim())) {
							text_sort.add(allname.get(nn));
							image_sort.add(allphone.get(nn));
							User_photo.add(allimage.get(nn));

							Log.i("shorted", "" + allname.get(nn));
						}
					}
				}
				ListView list = (ListView) rootView
						.findViewById(R.id.getallcontacts);
				list.setAdapter(new MyCustomAdapter(text_sort, image_sort,
						User_photo));
			}
		});
		new CreateNewProduct().execute();
		timer = new Thread() {

			public void run() {
				running = true;

				while (running) {
					try {
						sleep(50000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} finally {
						Log.i("thred", "thred");
						new CreateNewProduct().execute();
					}
				}
			}
		};
		timer.start();

		return rootView;
	}

	class CreateNewProduct extends AsyncTask<String, String, String> {
		JSONArray contacts = null;

		protected String doInBackground(String... args) {
			allname.removeAll(allname);
			allphone.removeAll(allphone);
			allimage.removeAll(allimage);
			SharedPreferences setting = context
					.getSharedPreferences("login", 0);
			authkey = setting.getString("authkey", "nothing");
			String status = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair("_ses", "" + authkey));
			JSONObject json = jsonParser.makeHttpRequest(url_recentchat, "GET",
					params);
			try {
				status = json.getString("status");
				String recent_chats = json.getString("recent_chats");
				JSONArray responcearray = new JSONArray(recent_chats);
				for (int i = 0; i < responcearray.length(); i++) {
					JSONObject jObject = responcearray.getJSONObject(i);
					group_id[i] = jObject.getString("group_id");
					String message = jObject.getString("message");
					String name = jObject.getString("name");
					String m = jObject.getString("profile_pic");
					String proper = m.replaceAll("\"", "");
					Log.i("User Link", "" + proper);
					String urllink = domain.getMainDomain(0);
					urllink += proper;

					Log.i("User Link 2nd time", "" + urllink);
					allname.add(name);
					allphone.add(message);
					allimage.add(urllink);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return status;
		}

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			ListView list = (ListView) rootView
					.findViewById(R.id.getallcontacts);
			list.setAdapter(new MyCustomAdapter(allname, allphone, allimage));

		}
	}

	class MyCustomAdapter extends BaseAdapter {
		String[] data_text;
		String[] data_image;
		String[] user_photo;

		MyCustomAdapter(String[] text, String[] image, String[] photo) {
			data_text = text;
			data_image = image;
			user_photo = photo;
		}

		MyCustomAdapter(ArrayList<String> text, ArrayList<String> image,
				ArrayList<String> photo) {
			data_text = new String[text.size()];
			data_image = new String[image.size()];
			user_photo = new String[image.size()];
			for (int i = 0; i < text.size(); i++) {
				data_text[i] = text.get(i);
				data_image[i] = image.get(i);
				user_photo[i] = photo.get(i);
			}

		}

		public int getCount() {
			return data_text.length;
		}

		public String getItem(int position) {
			return null;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = getLayoutInflater(null);
			View row;

			row = inflater.inflate(R.layout.recentchat, parent, false);

			TextView textview = (TextView) row.findViewById(R.id.textView1);
			TextView phone = (TextView) row.findViewById(R.id.textView2);

			ImageView profileImage = (ImageView) row
					.findViewById(R.id.userImage);
			textview.setText(data_text[position]);
			phone.setText(data_image[position]);

			imageLoader.DisplayImage(user_photo[position], profileImage);
			return (row);
		}
	}

	@Override
	public void onPause() {
		super.onPause();
		running = false;
	}

	class DeleteConversation extends AsyncTask<String, String, String> {

		protected String doInBackground(String... args) {

			SharedPreferences setting = context
					.getSharedPreferences("login", 0);
			authkey = setting.getString("authkey", "nothing");
			String status = null;
			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair("_action", "delete_conversation"));
			params.add(new BasicNameValuePair("group_id", args[0]));
			params.add(new BasicNameValuePair("_ses", authkey));

			JSONObject json = jsonParser.makeHttpRequest(
					"http://chat.scubeg.com/", "POST", params);
			return args[0];
		}

		// api/chats/delete/<group_id>

		@Override
		protected void onPostExecute(String result) {
			super.onPostExecute(result);
			Toast.makeText(context, "deleted" + result,
					Toast.LENGTH_SHORT).show();
			Log.i("url for deleteing", "" + url_delete_conversation);
		}
	}

//	public void onClick(View v) {
//
//		switch (v.getId()) {
//		case R.id.menu_user:
//
//			Intent intent = new Intent(Chat.this, TabSample.class);
//			startActivity(intent);
//
//			break;
//		case R.id.menuViewHomeButton:
//
//			showAlertDialog(this, "R u Sure to Logout nirmal ?" + "",
//					"R u Sure to logout ?", false);
//
//			break;
//		}
//
//	}

	@SuppressWarnings("deprecation")
	public void showAlertDialog(final Context context, String title, String message,
			Boolean status) {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);

		// Setting Dialog Title
		alertDialog.setTitle(title);

		// Setting Dialog Message
		// alertDialog.setMessage(message);

		// Setting Icon to Dialog
		alertDialog.setIcon(R.drawable.logo_title);

		// Setting Positive "Yes" Button
		alertDialog.setPositiveButton("YES",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						SharedPreferences Automaticlogin = context.getSharedPreferences(
								"automaticlogin", 0);
						SharedPreferences.Editor Automaticloginedit = Automaticlogin
								.edit();
						Automaticloginedit.putString("phone", "");
						Automaticloginedit.putString("pass", "");
						Automaticloginedit.commit();
						Intent login = new Intent(context, Login.class);
						startActivity(login);
						//finish();
					}
				});

		// Setting Negative "NO" Button
		alertDialog.setNegativeButton("NO",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						// Write your code here to invoke NO event
						dialog.cancel();
					}
				});

		// Showing Alert Message
		alertDialog.show();

	}

@Override
public void onClick(View v) {
	
}

}