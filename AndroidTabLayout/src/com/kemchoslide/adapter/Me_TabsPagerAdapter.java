package com.kemchoslide.adapter;

import com.kemchoslide.Me_Education_Fragment;
import com.kemchoslide.Me_Intrest_Fragment;
import com.kemchoslide.Me_Work_Fragment;
import com.kemchoslide.Me_Profile_fragment;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class Me_TabsPagerAdapter extends FragmentPagerAdapter {

	public Me_TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			// Top Rated fragment activity
			return new Me_Profile_fragment();
		case 1:
			// Games fragment activity
			return new Me_Education_Fragment();
		case 2:
			// Movies fragment activity
			return new Me_Work_Fragment();
		case 3:
			// Movies fragment activity
			return new Me_Intrest_Fragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 4;
	}

}
