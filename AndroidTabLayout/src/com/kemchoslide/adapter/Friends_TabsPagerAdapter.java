package com.kemchoslide.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kemchoslide.Friends_FaceBook_Fragment;
import com.kemchoslide.Friends_Kemcho_Fragment;
import com.kemchoslide.Friends_Mobile_Fragment;
import com.kemchoslide.Friends_Sponcered_Fragment;
import com.kemchoslide.Me_Education_Fragment;
import com.kemchoslide.Me_Intrest_Fragment;
import com.kemchoslide.Me_Profile_fragment;
import com.kemchoslide.Me_Work_Fragment;

public class Friends_TabsPagerAdapter extends FragmentPagerAdapter {

	public Friends_TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {

		switch (index) {
		case 0:
			return new Friends_FaceBook_Fragment();
		case 1:
			return new Friends_Mobile_Fragment();
		case 2:
			return new Friends_FaceBook_Fragment();
		case 3:
			return new Friends_Sponcered_Fragment();
		}

		return null;
	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 4;
	}

}
